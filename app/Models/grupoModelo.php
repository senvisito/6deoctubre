<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of grupoModelo
 *
 * @author a023942078k
 */

namespace App\Models;
use CodeIgniter\Model;


class grupoModelo extends Model {
    /* definimos los parámetros de la tabla a la que
     * queremos acceder
     */
    protected $table = 'grupos';
    protected $primaryKey = 'id';
    //protected $returnType = 'object';
    
}
